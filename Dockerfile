#
# Java (Oracle Java 7 JDK) + SSH Dockerfile
#
# Version               0.0.0

# Pull base image.
FROM dockerfile/java:oracle-java7

# Define maintainer
MAINTAINER lukaci

# Install SSH
RUN apt-get update && apt-get install -y openssh-server

# Configure SSH
RUN mkdir /var/run/sshd
RUN echo 'root:root' | chpasswd
RUN sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# Expose SSH
EXPOSE 22

# Start SSH
CMD ["/usr/sbin/sshd", "-D"]
